package com.example.musicmood.userdb.repository;

import com.example.musicmood.userdb.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDBRepository extends CrudRepository<User, Long> {
    public User getUserByUsername(String name);
}
