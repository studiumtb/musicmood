package com.example.musicmood.userdb.controller;

import com.example.musicmood.userdb.entity.LoginRequest;
import com.example.musicmood.userdb.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/User")
@RequiredArgsConstructor
public class UserController {
    private final UserService service;

    @DeleteMapping
    public ResponseEntity wipeDB()
    {
        log.info("Wipe DB");
        service.wipeDB();
        return ResponseEntity.ok().build();
    }

    /**
     * Log in to the System
     * @param username
     * @param password
     * @return Token for Requests to all Services in the System
     */
    @GetMapping("/user/{username}")
    public ResponseEntity<String> login(@PathVariable String username, @RequestParam String password)
    {
        log.info("User " + username + " tries to login");

        if(!service.isUserLoggedIn(username))
        {
            String result = service.login(username, password);
            if (! result.equals(""))
                return ResponseEntity.ok(result);
            else
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        else
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }

    /**
     * Logout of the System. Invalidate Session Token
     * @param id
     * @return
     */
    @DeleteMapping("/token/{id}")
    public ResponseEntity logout(@PathVariable String id)
    {
        log.info("User tries to logout");

        boolean result = service.logout(id);
        if(result)
            return ResponseEntity.ok().build();
        else
            return ResponseEntity.notFound().build();
    }

    /**
     * Create new User
     * @param username
     * @param password
     * @return
     */
    @PostMapping("/user/{username}")
    public ResponseEntity create(@PathVariable String username, @RequestParam String password)
    {
        log.info("Create User " + username);

        boolean result = service.createUser(username, password);
        if(result)
            return ResponseEntity.ok().build();
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @DeleteMapping("/user/{username}")
    public ResponseEntity delete(@PathVariable String username)
    {
        log.info("Delete User " + username);

        boolean result = service.deleteUser(username);
        if(result)
            return ResponseEntity.ok().build();
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    /**
     * Get User from Token
     * @param id
     * @return User Id
     */
    @GetMapping("/token/{id}")
    public ResponseEntity<Long> getUser(@PathVariable String id)
    {
        log.info("Get User for Token " + id);

        long result = service.getUserIdForToken(id);
        if(result > -1)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @GetMapping("/user")
    public ResponseEntity<Iterable<String>> getUserList()
    {
        log.info("Get User List");

        return ResponseEntity.ok(service.getUserList());
    }
}
