package com.example.musicmood.userdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserDBApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserDBApplication.class, args);
    }
}
