package com.example.musicmood.userdb.service;

import com.example.musicmood.userdb.entity.Token;
import com.example.musicmood.userdb.entity.User;
import com.example.musicmood.userdb.repository.UserDBRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class UserService extends UserServiceBaseData
{
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final int TOKEN_SIZE = 20;
    private static final int TOKEN_LIFETIME_IN_MINUTES = 20;

    @Autowired
    private final UserDBRepository users;

    public byte[] getSalt(String username) { return users.getUserByUsername(username).getSalt(); }
    public String login(String username, byte[] pwHashAndSalt)
    {
        var compare = users.getUserByUsername(username);
        if(compare.getPwhash().equals(pwHashAndSalt))
        {
            String token = generateRandomToken();
            sessionTokens.add(new Token(token,compare.getId()));
            return token;
        }
        else
            return "";
    }
    public Boolean isUserLoggedIn(String username)
    {
        var user = users.getUserByUsername(username);
        if(user != null)
        {
            if(sessionTokens.stream().filter(t -> t.getUserId() == user.getId() && isTokenValid(t)).findAny().isPresent())
                return true;
            else
                return false;
        }
        else
            return false;
    }

    public String login(String username, String password)
    {
        var compare = users.getUserByUsername(username);
        if(compare != null && isExpectedPassword(password.toCharArray(),compare.getSalt(),compare.getPwhash()))
        {
            String token = generateRandomToken();
            sessionTokens.add(new Token(token,compare.getId()));
            return token;
        }
        else
            return "";
    }
    public Boolean logout(String token)
    {
        return sessionTokens.removeIf(t -> t.getToken().equals(token));
    }
    public long getUserIdForToken(String token)
    {
        var utoken = sessionTokens.stream().filter(t -> t.getToken().equals(token)).findFirst();
        if(utoken.isPresent())
        {
            var ltoken = utoken.get();
            if(isTokenValid(ltoken)) {
                ltoken.updateAccessed();
                return ltoken.getUserId();
            }
            else
            {
                sessionTokens.remove(ltoken);
                return -1;
            }
        }
        else
            return -1;
    }

    private boolean isTokenValid(Token t)
    {
        if(!t.getLastAccessed().isBefore(LocalDateTime.now().minusMinutes(TOKEN_LIFETIME_IN_MINUTES)))
            return true;
        else
        {
            sessionTokens.remove(t);
            return false;
        }
    }

    public boolean createUser(String username, String password)
    {
        if(users.getUserByUsername(username) != null)
            return false;

        User user = new User();
        user.setUsername(username);

        byte[] salt = new byte[20];
        random.nextBytes(salt);
        user.setSalt(salt);

        byte[] passwordHash = hash(password.toCharArray(),salt);
        user.setPwhash(passwordHash);

        users.save(user);
        return true;
    }

    public List<String> getUserList()
    {
        return StreamSupport.stream(users.findAll().spliterator(),false).map(u -> u.getUsername()).collect(Collectors.toList());
    }

    public boolean deleteUser(String name)
    {
        var user = users.getUserByUsername(name);
        if(user != null)
        {
            users.delete(user);
            return true;
        }
        else
            return false;
    }

    public void wipeDB()
    {
        users.deleteAll();
    }

    private static String generateRandomToken() {
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < TOKEN_SIZE; i++)
        {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public static byte[] hash(char[] password, byte[] salt) {
        PBEKeySpec spec = new PBEKeySpec(password, salt, 5, 256);
        Arrays.fill(password, Character.MIN_VALUE);
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            return skf.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new AssertionError("Error while hashing a password: " + e.getMessage(), e);
        } finally {
            spec.clearPassword();
        }
    }
    public static boolean isExpectedPassword(char[] password, byte[] salt, byte[] expectedHash) {
        byte[] pwdHash = hash(password, salt);
        Arrays.fill(password, Character.MIN_VALUE);
        if (pwdHash.length != expectedHash.length) return false;
        for (int i = 0; i < pwdHash.length; i++) {
            if (pwdHash[i] != expectedHash[i]) return false;
        }
        return true;
    }
}

class UserServiceBaseData
{
    protected Random random = new SecureRandom();
    protected List<Token> sessionTokens = new LinkedList<>();
}
