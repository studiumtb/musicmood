package com.example.musicmood.userdb.entity;

import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class Token {
    private String token;
    private LocalDateTime lastAccessed;
    private long userId;

    public Token(String token, long userId)
    {
        this.token = token;
        this.userId = userId;
        lastAccessed = LocalDateTime.now();
    }

    public void updateAccessed()
    {
        lastAccessed = LocalDateTime.now();
    }
}
