package com.example.musicmood.gateway.entities;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Song
{
    private int id;

    private String name;
    private String artist;
    private double length;
}