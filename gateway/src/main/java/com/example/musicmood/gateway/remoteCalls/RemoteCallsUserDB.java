package com.example.musicmood.gateway.remoteCalls;

import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.rmi.ServerException;

/**
 * Verbindung zum User DB Service
 */
@Slf4j
public class RemoteCallsUserDB {
    /**
     * Erhalte Nutzer Id aus Session Token
     * @param token
     * @return
     * @throws IOException
     * @throws NotFoundException
     */
    public long getUserFromToken(String token) throws IOException, NotFoundException {
        URL url = new URL("http://localhost:8081/User/token/" + token);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("GET");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            String body = readBody(request);
            try
            {
                return Long.parseLong(body);
            }
            catch (NumberFormatException ex)
            {
                throw new ServerException("Invalid Response");
            }
        }
        else
        {
            throw new NotFoundException("Token Invalid");
        }
    }

    /**
     * Logge Nutzer ins System ein
     * @param username
     * @param password
     * @return Session Token
     * @throws IOException
     * @throws NotFoundException
     * @throws ServerException
     */
    public String login(String username, String password) throws IOException, NotFoundException, ServerException {
        URL url = new URL("http://localhost:8081/User/user/" + username + "?password=" + password);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("GET");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            return readBody(request);
        }
        else if(status == HttpStatus.FORBIDDEN.value())
        {
            throw new NotFoundException("Invalid User or Password");
        }
        else if(status == HttpStatus.CONTINUE.value())
        {
            throw new FileAlreadyExistsException("Already logged in");
        }
        else
            throw new ServerException("Service Error");
    }

    /**
     * Lösche Session Token
     * @param token
     * @return
     * @throws IOException
     */
    public Boolean logout(String token) throws IOException {
        URL url = new URL("http://localhost:8081/User/token/" + token);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("DELETE");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            return true;
        }
        else if(status == HttpStatus.NOT_FOUND.value())
        {
            return false;
        }
        else
        {
            throw new ServerException("Internal Service Error");
        }
    }

    /**
     * Lege Nutzer an
     * @param username
     * @param password
     * @return
     * @throws IOException
     */
    public Boolean create(String username, String password) throws IOException {
        URL url = new URL("http://localhost:8081/User/user/" + username + "?password=" + password);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("POST");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            return true;
        }
        else if(status == HttpStatus.BAD_REQUEST.value())
        {
            return false;
        }
        else
        {
            throw new ServerException("Internal Service Error");
        }
    }

    private String readBody(HttpURLConnection response) throws IOException {
        InputStream input = response.getInputStream();
        BufferedReader in = new BufferedReader(new InputStreamReader(input));
        StringBuilder bodyContent = new StringBuilder();
        String currentLine;

        while((currentLine = in.readLine()) != null)
            bodyContent.append(currentLine);

        in.close();
        return bodyContent.toString();
    }
}
