package com.example.musicmood.gateway.entities;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SongRating
{
    private int SongId;

    private int Funny_Sad;
    private int Light_Deep;
    private int Hard_Soft;

    public void SetRating(int funny_sad, int light_deep, int hard_soft)
    {
        Funny_Sad = funny_sad;
        Light_Deep = light_deep;
        Hard_Soft = hard_soft;
    }
}
