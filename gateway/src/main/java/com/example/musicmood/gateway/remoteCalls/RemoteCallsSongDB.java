package com.example.musicmood.gateway.remoteCalls;

import com.example.musicmood.gateway.entities.Song;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.rmi.ServerException;
import java.util.*;

/**
 * Verbindung zum Song DB Service
 */
@Slf4j
public class RemoteCallsSongDB {
    JSONParser parser = new JSONParser();

    /**
     * Fordere Liste aller gespeicherten Lieder an
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public Iterable<Song> getSongs() throws IOException, ParseException {
        URL url = new URL("http://localhost:8082/Songs");
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("GET");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            List<Song> songList = new LinkedList<Song>();
            JSONArray songs = (JSONArray) parser.parse(new InputStreamReader(request.getInputStream()));
            songs.forEach(item -> {
                JSONObject obj = (JSONObject) item;
                Song parsed = parseSong(obj);
                songList.add(parsed);
            });

            return songList;
        }
        else
        {
            throw new ServerException("Invalid Response");
        }
    }

    /**
     * Fordere Lied Informationen anhand seiner Systeminternen Id an
     * @param id Systeminterne Id des Lieds
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public Song getSong(Long id) throws IOException, ParseException {
        URL url = new URL("http://localhost:8082/Songs/id/" + id);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("GET");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            JSONObject jsong = (JSONObject)parser.parse(new InputStreamReader(request.getInputStream()));
            return parseSong(jsong);
        }
        else if(status == HttpStatus.NOT_FOUND.value())
        {
            log.warn("Song {} not found", id);
            return null;
        }
        else
        {
            return null;
        }
    }

    /**
     * Fordere Lied Informationen anhand Titel und Künstlername an
     * @param name
     * @param artist
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public Song getSong(String name, String artist) throws IOException, ParseException {
        name = name.replace(' ','_');

        URL url = new URL("http://localhost:8082/Songs/name/" + artist + "/" + name);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("GET");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            JSONObject jsong = (JSONObject)parser.parse(new InputStreamReader(request.getInputStream()));
            return parseSong(jsong);
        }
        else if(status == HttpStatus.NOT_FOUND.value())
        {
            log.warn("Song {} not found", name);
            return null;
        }
        else
        {
            return null;
        }
    }

    /**
     * Füge Lied zur Song DB hinzu
     * @param name Song Titel
     * @param artist Künstler Name
     * @param length Song Länge
     * @return
     * @throws IOException
     */
    public boolean addSong(String name, String artist, Double length) throws IOException {
        name = name.replace(' ','_');

        URL url = new URL("http://localhost:8082/Songs/name/" + artist + "/" + name + "?length=" + length);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("POST");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private Song parseSong(JSONObject jsong)
    {
        Integer id = ((Long)jsong.get("id")).intValue();
        String name = (String)jsong.get("name");
        String artist = (String)jsong.get("artist");
        Double length = (Double)jsong.get("length");

        return new Song(id,name,artist,length);
    }
}