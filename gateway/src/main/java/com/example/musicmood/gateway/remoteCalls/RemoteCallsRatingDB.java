package com.example.musicmood.gateway.remoteCalls;

import com.example.musicmood.gateway.entities.SongRating;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.rmi.ServerException;
import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.List;

/**
 * Verbindung zur Rating DB
 */
@Slf4j
public class RemoteCallsRatingDB {
    JSONParser parser = new JSONParser();

    /**
     * Fordere Bewertung für Lied von Nutzer an
     * @param songId
     * @param usertoken
     * @return
     * @throws IOException
     * @throws IllegalAccessException
     */
    public SongRating getSongRating(long songId, String usertoken) throws IOException, IllegalAccessException {
        log.info("GET Song Rating from RatingDB");

        URL url = new URL("http://localhost:8083/Rating/Song/" + songId + "/" + usertoken);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("GET");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(request.getInputStream(), SongRating.class);
        }
        else if(status == HttpStatus.UNAUTHORIZED.value())
        {
            log.warn("Invalid User Token");
            throw new IllegalAccessException("Invalid User Token");
        }
        else
        {
            log.error("Internal Service Error");
            throw new ServerException("Internal Service Error");
        }
    }

    /**
     * Fordere Durchschnittsbewertung für Lied an
     * @param songId Systeminteren ID des Lieds
     * @return
     * @throws IOException
     * @throws NotFoundException
     */
    public SongRating getSongRating(long songId) throws IOException, NotFoundException {
        log.info("GET Song Rating from RatingDB");

        URL url = new URL("http://localhost:8083/Rating/song/" + songId );
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("GET");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return mapper.readValue(request.getInputStream(), SongRating.class);
        }
        else if(status == HttpStatus.NOT_FOUND.value())
        {
            throw new NotFoundException("Not Rating for Song found");
        }
        else
        {
            log.error("Internal Service Error");
            throw new ServerException("Internal Service Error");
        }
    }

    /**
     * Füge Bewertung zu System hinzu
     * @param songId System Interne Id des Lieds
     * @param usertoken Session Token des Nutzers
     * @param funny_sad Fröhlich-Traurig Bewertung
     * @param light_deep Seicht-Tiefgreifend Bewertung
     * @param hard_soft Hart-Leicht Bewertung
     * @throws IOException
     * @throws IllegalAccessException
     */
    public void addRating(long songId, String usertoken, int funny_sad, int light_deep, int hard_soft) throws IOException, IllegalAccessException {
        log.info("POST Song Rating to RatingDB");

        URL url = new URL("http://localhost:8083/Rating/song/" + songId + "/" + usertoken + "?funny_sad=" + funny_sad + "&light_deep=" + light_deep + "&hard_soft=" + hard_soft);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("POST");

        int status = request.getResponseCode();
        if(status != HttpStatus.OK.value())
        {
            if(status == HttpStatus.BAD_REQUEST.value())
            {
                log.error("Request had Invalid Parameters");
                throw new InvalidParameterException();
            }
            else if(status == HttpStatus.UNAUTHORIZED.value())
            {
                log.error("Invalid User Token");
                throw new IllegalAccessException("Invalid User Token");
            }
            else
            {
                log.error("Internal Service Error");
                throw new ServerException("Internal Service Error");
            }
        }
    }

    /**
     * Lösche Nutzerbewertung aus dem System
     * @param songId System Interne Id des Lieds
     * @param usertoken Session Token des Nutzers
     * @throws IOException
     * @throws IllegalAccessException
     * @throws NotFoundException
     */
    public void removeRating(long songId, String usertoken) throws IOException, IllegalAccessException, NotFoundException {
        log.info("DELETE Song Rating from RatingDB");

        URL url = new URL("http://localhost:8083/Rating/Song/" + songId + "/" + usertoken);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("DELETE");

        int status = request.getResponseCode();
        if(status != HttpStatus.OK.value())
        {
            if(status == HttpStatus.NOT_FOUND.value())
            {
                log.warn("No Rating for Song by requested User");
                throw new NotFoundException("No Rating for Song by requested User");
            }
            if(status == HttpStatus.UNAUTHORIZED.value())
            {
                log.warn("Invalid User Token");
                throw new IllegalAccessException("Invalid User Token");
            }
        }
    }

    /**
     * Erstelle Playlist anhand der gegebenen Kriterien
     * @param funny_sad Fröhlich-Traurig Bewertung
     * @param light_deep Seicht-Tiefgreifend Bewertung
     * @param hard_soft Hart-Leicht Bewertung
     * @param count Maximal Anzahl der Lieder in der Playlist
     * @param threshold Maximale Bewertungs Abweichtung
     * @return Liste an Song Ids
     * @throws IOException
     * @throws ParseException
     */
    public Iterable<Long> getSongsForMood(int funny_sad, int light_deep, int hard_soft, Integer count, Integer threshold) throws IOException, ParseException {
        if(funny_sad < 0 || funny_sad > 100 || light_deep < 0 || light_deep > 100 || hard_soft < 0 || hard_soft > 100 || count <= 0 || threshold < 0 || threshold > 100)
            throw new InvalidParameterException();

        URL url = new URL("http://localhost:8083/Rating/playlist?funny_sad=" + funny_sad + "&light_deep=" + light_deep + "&hard_soft=" + hard_soft
                            + "&count=" + count + "&threshold=" + threshold);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("GET");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            List<Long> songIdList = new LinkedList<Long>();
            JSONArray songs = (JSONArray) parser.parse(new InputStreamReader(request.getInputStream()));
            songs.forEach(item -> {
                long songId = (long)item;
                songIdList.add(songId);
            });
            return songIdList;
        }
        else
        {
            throw new IOException();
        }
    }
}
