package com.example.musicmood.gateway.controller;

import com.example.musicmood.gateway.entities.Song;
import com.example.musicmood.gateway.entities.SongRating;
import com.example.musicmood.gateway.entities.SongWithRating;
import com.example.musicmood.gateway.remoteCalls.RemoteCallsRatingDB;
import com.example.musicmood.gateway.remoteCalls.RemoteCallsSongDB;
import com.example.musicmood.gateway.remoteCalls.RemoteCallsUserDB;
import com.example.musicmood.gateway.service.GatewayService;

import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.rmi.ServerException;
import java.util.LinkedList;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/Gateway")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class GatewayController {
    private final GatewayService service;

    //Connections to the Database Services
    private RemoteCallsUserDB userDB = new RemoteCallsUserDB();
    private RemoteCallsSongDB songDB = new RemoteCallsSongDB();
    private RemoteCallsRatingDB ratingDB = new RemoteCallsRatingDB();

    /**
     * Nutzer möchte sich im System anmelden
     * @param username
     * @param password
     * @return einen Session Token
     */
    @GetMapping("/tokens/{username}")
    public ResponseEntity<String> login(@PathVariable(value = "username") String username, @RequestParam(value = "password") String password)
    {
        log.info("User {} tries to Login", username);
        try
        {
            //Fordere Session Token vom User DB Service an
            String token = userDB.login(username, password);
            log.info("User {} logged in", username);
            return ResponseEntity.ok(token);
        }
        catch (NotFoundException nfox)
        {
            log.warn("Wrong Username or Password for User {}", username);
            return ResponseEntity.notFound().build();
        }
        catch (ServerException sox)
        {
            log.warn("User Service returned Internal error {}", sox.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        catch (FileAlreadyExistsException fex)
        {
            log.warn("User already logged in");
            return ResponseEntity.status(403).build();
        }
        catch (IOException iox)
        {
            log.warn("Error accessing User Service {}", iox.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Invalidiere Session Token
     * @param token
     * @return
     */
    @DeleteMapping("/tokens/{token}")
    public ResponseEntity logout(@PathVariable(value = "token") String token)
    {
        log.info("Logout User");
        try
        {
            //Lösche Session Token aus dem User DB Service
            Boolean result = userDB.logout(token);
            if(result)
                return ResponseEntity.ok().build();
            else
                return ResponseEntity.notFound().build();
        }
        catch (IOException iox)
        {
            log.warn("Error on Logout {}", iox.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Erstelle neuen Nutzer im System
     * @param username
     * @param password
     * @return
     */
    @PostMapping("/users/{username}")
    public ResponseEntity create(@PathVariable(value = "username") String username, @RequestParam(value = "password") String password)
    {
        log.info("Create User {}",username);
        try
        {
            //Erstelle neuen Nutzer auf dem User DB Service
            Boolean result = userDB.create(username,password);
            if(result)
                return ResponseEntity.ok().build();
            else
                return ResponseEntity.notFound().build();
        }
        catch (IOException e)
        {
            log.warn("Error on Create User {}" + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Fordere Liste aller gespeicherten Lieder und ihrer Durchschnittsbewertung an
     * @return
     */
    @GetMapping("/songs")
    public ResponseEntity<Iterable<SongWithRating>> getAllSongs()
    {
        log.info("Get full Song list");
        try
        {
            List<SongWithRating> allRatings = new LinkedList<SongWithRating>();
            //Fordere Liste aller Lieder vom Song DB Service an
            var allSongs = songDB.getSongs();
            //Für alle Lieder...
            allSongs.forEach(s -> {
                try {
                    //Fordere Durchschnittsbewertung vom Rating DB Service an
                    SongRating dbrating = ratingDB.getSongRating(s.getId());
                    //Erstelle zusammengesetzten Datensatz
                    SongWithRating rating = new SongWithRating(s.getName(),s.getArtist(),s.getLength(),dbrating.getFunny_Sad(),dbrating.getLight_Deep(),dbrating.getHard_Soft());
                    //Füge Datensatz zur Liste hinzu
                    allRatings.add(rating);
                } catch (IOException e) {
                    log.warn("Error on getting Song from SongDB " + e);
                }
                catch (NotFoundException nex) {
                    log.info("No Ratings for Song Found");
                }
            });

            //Gebe Liste zurück
            return ResponseEntity.ok(allRatings);
        }
        catch (ParseException pex)
        {
            log.warn("Error on getting full Song list " + pex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        catch (IOException iox)
        {
            log.warn("Error on getting full Song list " + iox.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Fordere gespeicherte Basisinformationen zu Lied an
     * @param name
     * @param artist
     * @return
     */
    @GetMapping("/songs/name/{name}/{artist}")
    public ResponseEntity<Song> getSongWithName(@PathVariable(value = "name") String name, @PathVariable(value = "artist") String artist)
    {
        log.info("Get Song {}", name);
        try
        {
            //Fordere Basisinformationen zu Lied von Song DB Service an
            var song = songDB.getSong(name,artist);
            if(song != null)
                return ResponseEntity.ok(song);
            else
                return ResponseEntity.notFound().build();
        }
        catch (IOException | ParseException e) {
            log.error("Error on get Song with name" + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Füge Basis Informationen zu Lied zum System hinzu
     * @param name
     * @param artist
     * @param length
     * @param userToken Session Token zur Nutzer verifizierung
     * @return
     */
    @PostMapping("/songs/name/{name}/{artist}")
    public ResponseEntity addSong(@PathVariable(value = "name") String name, @PathVariable(value = "artist") String artist,
                                  @RequestParam(value = "length") Double length, @RequestParam(value = "usertoken") String userToken)
    {
        log.info("Add Song " + name + " from " + artist);
        try
        {
            //Ist der Nutzer Token gültig?
            userDB.getUserFromToken(userToken);

            //Existiert das Lied noch nicht in der Song DB
            if(songDB.getSong(name,artist) == null) {
                //Füge Lied zu Song DB hinzu
                songDB.addSong(name, artist, length);
            }
            return ResponseEntity.ok().build();
        }
        catch (IOException | ParseException e) {
            e.printStackTrace();
            log.warn("Error on add Song " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        catch (NotFoundException nex)
        {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Fordere Rating von Song über seine systeminterne Id an
     * @param songId
     * @return
     */
    @GetMapping("/ratings/song/id/{id}")
    public ResponseEntity<SongWithRating> getRatingForSongWithId(@PathVariable(value = "id") Long songId) {
        log.info("Get Rating for Song " + songId);
        try
        {
            //Fordere Basis Informationen zu Lied von Song DB an
            Song song = songDB.getSong(songId);
            //Fordere Bewertung zu Lied von Rating DB an
            SongWithRating result = getRatingForSong(song);
            return ResponseEntity.ok(result);
        }
        catch (IOException iox)
        {
            log.warn("Error on get Rating for Song With Id " + iox.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        catch (ParseException pox)
        {
            log.warn("Error on get Rating for Song With Id " + pox.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        catch (NotFoundException e)
        {
            log.warn("No Rating for Song");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    /**
     * Fordere durchschnitts Bewertung für einen Spezielles Lied an
     * @param name
     * @param artist
     * @return Vollständiger Datensatz des Liedes
     */
    @GetMapping("/ratings/song/name/{name}/{artist}")
    public ResponseEntity<SongWithRating> getRatingForSongWithName(@PathVariable(value = "name") String name, @PathVariable(value = "artist") String artist)
    {
        log.info("Get Rating for Song " + name + " from " + artist);
        try
        {
            //Fordere Basis Informationen zu Lied von Song DB an
            Song song = songDB.getSong(name,artist);
            //Fordere Bewertung zu Lied von Rating DB an
            var result = getRatingForSong(song);

            return ResponseEntity.ok(result);
        }
        catch (IOException iox)
        {
            log.warn("Error on get Rating for Song With Name " + iox.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        catch (ParseException pox)
        {
            log.warn("Error on get Rating for Song With Name " + pox.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (NotFoundException e)
        {
            log.warn("No Rating for Song");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    /**
     * Füge Bewertung für Lied hinzu
     * @param songid Systeminterne Id des Liedes
     * @param usertoken Session Token des Nutzers
     * @param funny_sad Fröhlich-Traurig Bewertung
     * @param light_deep Seicht-Tiefgreifend Bewertung
     * @param hard_soft Hart-Leicht Bewertung
     * @return
     */
    @PostMapping("/ratings/song/id/{songid}")
    public ResponseEntity addRatingWithId(@PathVariable(value = "songid") long songid, @RequestParam(value = "usertoken") String usertoken,
                                    @RequestParam(value = "funny_sad") int funny_sad, @RequestParam(value = "light_deep") int light_deep, @RequestParam(value = "hard_soft") int hard_soft)
    {
        try
        {
            //Existiert das Lied im System
            if(songDB.getSong(songid) != null)
            {
                //Füge Bewertung zu Song DB hinzu
                ratingDB.addRating(songid, usertoken, funny_sad, light_deep, hard_soft);
                return ResponseEntity.ok().build();
            }
            else
                return ResponseEntity.notFound().build();
        }
        catch (IllegalAccessException ix)
        {
            log.warn("User tried to add Rating with Invalid Token");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        catch (Exception ex)
        {
            log.warn("Error on Post Rating " + ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Füge Bewertung für Lied hinzu, Identifiziere es über Titel und Künstler
     * @param name Titel des Lieds
     * @param artist Name des Künstlers
     * @param usertoken Session Token des Nutzers
     * @param funny_sad Fröhlich-Traurig Bewertung
     * @param light_deep Seicht-Tiefgreifend Bewertung
     * @param hard_soft Hart-Leicht Bewertung
     * @param create Soll das Lied erzeugt werden, falls es noch nicht im System existiert
     * @param length Läge des Lieds
     * @return
     */
    @PostMapping("/ratings/song/name/{name}/{artist}")
    public ResponseEntity addRatingWithName(@PathVariable(value = "name") String name, @PathVariable(value = "artist") String artist, @RequestParam(value = "usertoken") String usertoken,
                                            @RequestParam(value = "funny_sad") int funny_sad, @RequestParam(value = "light_deep") int light_deep, @RequestParam(value = "hard_soft") int hard_soft,
                                            @RequestParam(value = "create", defaultValue = "true") boolean create, @RequestParam(value = "length") double length)
    {
        try
        {
            log.info("ADD Rating");

            Song song = songDB.getSong(name,artist);

            if(song == null && create)
            {
                songDB.addSong(name, artist, length);
                song = songDB.getSong(name, artist);
            }

            if(song != null)
            {
                ratingDB.addRating(song.getId(), usertoken, funny_sad, light_deep, hard_soft);
                return ResponseEntity.ok().build();
            }
            else
                return ResponseEntity.notFound().build();
        }
        catch (IllegalAccessException ix)
        {
            log.warn("User tried to add Rating with Invalid Token");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        catch (Exception ex)
        {
            log.warn("Error on Post Rating " + ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Lösche Bewertung für Lied von Nutzer
     * @param songid
     * @param usertoken
     * @return
     */
    @DeleteMapping("/ratings/song/id/{id}")
    public ResponseEntity deleteRatingWithId(@PathVariable(value = "id") long songid, @RequestParam(value = "usertoken") String usertoken)
    {
        try
        {
            //Lösche Bewertung aus der Rating DB
            ratingDB.removeRating(songid,usertoken);
            return ResponseEntity.ok().build();
        }
        catch (IllegalAccessException e)
        {
            log.warn("User tried to delete Rating with Invalid Token");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        catch (IOException ex)
        {
            log.warn("Error on delete Rating " + ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        catch (NotFoundException nex)
        {
            log.warn("Tried to delete non existent rating");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Lösche Bewertung für Lied von Nutzer, Identifiziere es über Titel und Künstler
     * @param name Titel des Liedes
     * @param artist Name des Künstlers
     * @param usertoken Token
     * @return
     */
    @DeleteMapping("/ratings/song/name/{name}/{artist}")
    public ResponseEntity deleteRatingWithName(@PathVariable(value = "name") String name, @PathVariable(value = "artist") String artist, @RequestParam(value = "usertoken") String usertoken)
    {
        try
        {
            Song song = songDB.getSong(name,artist);
            if(song != null)
            {
                ratingDB.removeRating(song.getId(), usertoken);
                return ResponseEntity.ok().build();
            }
            else
                return ResponseEntity.notFound().build();
        }
        catch (IllegalAccessException e)
        {
            log.warn("User tried to delete Rating with Invalid Token");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        catch (IOException | ParseException ex)
        {
            log.warn("Error on delete Rating " + ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (NotFoundException e)
        {
            log.warn("Tried to delete non existent rating");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Generiere Playlist anhand der Kriterien des Nutzers
     * @param funny_sad Fröhlich-Traurig Bewertung
     * @param light_deep Seicht-Tiefgreifend Bewertung
     * @param hard_soft Hart-Leicht Bewertung
     * @param count Maximale Anzahl an Liedern in der Playlist
     * @param threshold Toleranz für Bewertungen
     * @return
     */
    @GetMapping("/playlist")
    public ResponseEntity<Iterable<SongWithRating>> getPlaylist(@RequestParam(value = "funny_sad") int funny_sad, @RequestParam(value = "light_deep") int light_deep, @RequestParam(value = "hard_soft") int hard_soft,
                                                      @RequestParam(value = "count") int count, @RequestParam(value = "threshold",defaultValue = "1") int threshold)
    {
        log.info("Get Playlist");
        try
        {
            //Fordere Playlist von Rating DB an
            var songs = ratingDB.getSongsForMood(funny_sad,light_deep,hard_soft,count,threshold);

            List<SongWithRating> playList = new LinkedList<>();
            //Lade Lieder Informationen von Song DB nach
            for(var songid : songs)
            {
                Song song = songDB.getSong(songid);
                playList.add(getRatingForSong(song));
            }

            return ResponseEntity.ok(playList);
        }
        catch (IOException e)
        {
            log.warn("Error on get Playlist " + e.getMessage());
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        catch (ParseException | NotFoundException e)
        {
            log.warn("Error on get Playlist " + e.getMessage());
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private SongWithRating getRatingForSong(Song song) throws IOException, NotFoundException {
        SongRating songrating = ratingDB.getSongRating(song.getId());
        if(songrating != null)
            return new SongWithRating(song.getName(), song.getArtist(), song.getLength(), songrating.getFunny_Sad(), songrating.getLight_Deep(), songrating.getHard_Soft());
        else
            return null;
    }
}
