package com.example.musicmood.ratingdb.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SongRating implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long RatingId;

    public long userId;
    public long songId;

    public int funny_Sad;
    public int light_Deep;
    public int hard_Soft;
}
