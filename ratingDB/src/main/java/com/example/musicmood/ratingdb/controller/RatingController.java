package com.example.musicmood.ratingdb.controller;

import com.example.musicmood.ratingdb.remoteCalls.RemoteCallsUserDB;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.example.musicmood.ratingdb.entity.*;
import com.example.musicmood.ratingdb.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@Slf4j
@RequestMapping("/Rating")
@RequiredArgsConstructor
public class RatingController {
    @Autowired
    private final RatingDBService service;

    private RemoteCallsUserDB userDB = new RemoteCallsUserDB();

    /**
     * Lösche vollständige Datenbank
     * @return
     */
    @DeleteMapping
    public ResponseEntity wipeDB()
    {
        log.info("Wipe full DB");
        service.wipeDB();
        return ResponseEntity.ok().build();
    }

    /**
     * Dumpe Vollständige Rating Liste
     * @return
     */
    @GetMapping
    public ResponseEntity<Iterable<SongRating>> getRatings()
    {
        log.info("GET Full Rating List");
        return ResponseEntity.ok(service.getAllRatings());
    }

    /**
     * Arhalte Rating für Song anhand seiner Systeminternen Id
     * @param songid
     * @param usertoken
     * @return
     */
    @GetMapping("/song/{song}/{usertoken}")
    public ResponseEntity<SongRating> getRating(@PathVariable(value = "song") long songid, @PathVariable(value = "usertoken") String usertoken)
    {
        try
        {
            //Hole Id des Token Inhabers
            long userid = userDB.getUserFromToken(usertoken);

            log.info("GET Rating of User {} for Song {}", userid, songid);
            var result = service.getRatingForSongOfUser(userid, songid);
            if (result.isPresent())
                return ResponseEntity.ok(result.get());
            else
                return ResponseEntity.notFound().build();
        }
        catch (NotFoundException ex)
        {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        catch (IOException ex)
        {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/song/{song}")
    public ResponseEntity<SongRating> getRating(@PathVariable(value = "song") long songid)
    {
        var rating = service.getSongRating(songid);
        if(rating.isPresent())
            return ResponseEntity.ok(rating.get());
        else
            return ResponseEntity.notFound().build();
    }

    @PostMapping("/song/{song}/{usertoken}")
    public ResponseEntity addRating(@PathVariable(value = "song") long songid, @PathVariable(value = "usertoken") String usertoken,
                                    @RequestParam(value = "funny_sad") int funny_sad, @RequestParam(value = "light_deep") int light_deep,
                                    @RequestParam(value = "hard_soft") int hard_soft)
    {
        if(funny_sad < 0 || funny_sad > 100 || light_deep < 0 || light_deep > 100 || hard_soft < 0 || hard_soft > 100)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mood Parameter out of Range");

        try
        {
            long userid = userDB.getUserFromToken(usertoken);

            service.addRating(userid,songid,funny_sad,light_deep,hard_soft);

            return ResponseEntity.ok().build();
        }
        catch (NotFoundException ex)
        {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        catch (IOException ex)
        {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/song/{song}/{usertoken}")
    public ResponseEntity removeRating(@PathVariable(value = "song") long songid, @PathVariable(value = "usertoken") String usertoken)
    {
        try
        {
            long userid = userDB.getUserFromToken(usertoken);
            boolean foundEntry = service.removeRating(userid,songid);

            if(foundEntry)
                return ResponseEntity.ok().build();
            else
                return ResponseEntity.notFound().build();
        }
        catch (NotFoundException ex)
        {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        catch (IOException ex)
        {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/song/{song}")
    public ResponseEntity removeRatings(@PathVariable(value = "song") long songid)
    {
        if (service.getSongRating(songid).isPresent()) {
            service.removerRatings(songid);
            return ResponseEntity.ok().build();
        } else
            return ResponseEntity.notFound().build();
    }

    @GetMapping("/playlist")
    public ResponseEntity<Iterable<Long>> getSongsForMood(@RequestParam("funny_sad") int funny_sad, @RequestParam("light_deep") int light_deep,
                                                          @RequestParam("hard_soft") int hard_soft, @RequestParam("count") int count, @RequestParam("threshold") int threshold)
    {
        if(funny_sad < 0 || funny_sad > 100 || light_deep < 0 || light_deep > 100 || hard_soft < 0 || hard_soft > 100 || count <= 0 || threshold < 0 || threshold > 100)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        var list = service.getSongsForMood(funny_sad,light_deep,hard_soft,count,threshold);
        return ResponseEntity.ok(list);
    }
}
