package com.example.musicmood.ratingdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RatingDBApplication {

    public static void main(String[] args) {
        SpringApplication.run(RatingDBApplication.class, args);
    }

}
