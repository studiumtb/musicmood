package com.example.musicmood.ratingdb.remoteCalls;

import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.rmi.ServerException;

@Slf4j
public class RemoteCallsUserDB {
    public long getUserFromToken(String token) throws IOException, NotFoundException {
        URL url = new URL("http://localhost:8081/User/token/" + token);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("GET");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            String body = readBody(request);
            try
            {
                return Long.parseLong(body);
            }
            catch (NumberFormatException ex)
            {
                throw new ServerException("Invalid Response");
            }
        }
        else
        {
            throw new NotFoundException("Token Invalid");
        }
    }

    private String readBody(HttpURLConnection response) throws IOException {
        InputStream input = response.getInputStream();
        BufferedReader in = new BufferedReader(new InputStreamReader(input));
        StringBuilder bodyContent = new StringBuilder();
        String currentLine;

        while((currentLine = in.readLine()) != null)
            bodyContent.append(currentLine);

        in.close();
        return bodyContent.toString();
    }
}