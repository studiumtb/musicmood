package com.example.musicmood.ratingdb.service;

import com.example.musicmood.ratingdb.entity.SongRating;
import com.example.musicmood.ratingdb.repository.RatingRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class RatingDBService {
    @Autowired
    private final RatingRepository ratings;

    public Iterable<SongRating> getAllRatings()
    {
        return ratings.findAll();
    }
    public Iterable<SongRating> getRatingsForSong(Long songId)
    {
        return ratings.findAllBySongId(songId);
    }
    public Optional<SongRating> getRatingForSongOfUser(Long userId, Long songId)
    {
        return ratings.findAllBySongIdAndUserId(songId,userId).stream().findAny();
    }

    public Integer getFunny_SadSongAvg(Long songId)
    {
        double avg = ratings.findAllBySongId(songId).stream().mapToInt(s -> s.funny_Sad).summaryStatistics().getAverage();
        return (Integer)(int)avg;
    }
    public Integer getLight_DeepSongAvg(Long songId)
    {
        double avg = ratings.findAllBySongId(songId).stream().mapToInt(s -> s.light_Deep).summaryStatistics().getAverage();
        return (Integer)(int)avg;
    }
    public Integer getHard_SoftSongAvg(Long songId)
    {
        double avg = ratings.findAllBySongId(songId).stream().mapToInt(s -> s.hard_Soft).summaryStatistics().getAverage();
        return (Integer)(int)avg;
    }
    public Integer getFunny_SadUserAvg(Long userId)
    {
        double avg = ratings.findAllByUserId(userId).stream().mapToInt(s -> s.funny_Sad).summaryStatistics().getAverage();
        return (Integer)(int)avg;
    }
    public Integer getLight_DeepUserAvg(Long userId)
    {
        double avg = ratings.findAllByUserId(userId).stream().mapToInt(s -> s.light_Deep).summaryStatistics().getAverage();
        return (Integer)(int)avg;
    }
    public Integer getHard_SoftUserAvg(Long userId)
    {
        double avg = ratings.findAllByUserId(userId).stream().mapToInt(s -> s.hard_Soft).summaryStatistics().getAverage();
        return (Integer)(int)avg;
    }

    public Optional<SongRating> getSongRating(Long songId)
    {
        if(ratings.findAllBySongId(songId).isEmpty())
            return Optional.empty();

        return Optional.of(new SongRating(-1,-1,songId,getFunny_SadSongAvg(songId),getLight_DeepSongAvg(songId),getHard_SoftSongAvg(songId)));
    }

    public Iterable<Long> getSongsForMood(Integer funny_sad, Integer light_deep, Integer hard_soft, Integer count, Integer threshold)
    {
        Assert.isTrue(funny_sad >= 0 && funny_sad <= 100 && light_deep >= 0 && light_deep <= 100 &&
                hard_soft >= 0 && hard_soft <= 100 && count > 0 && threshold >= 0 && threshold <= 100);

        Iterable<SongRating> songs = ratings.findAll();
        var ratingStream = StreamSupport.stream(songs.spliterator(), false).mapToLong(s -> s.getSongId()).distinct();
        var fitting = ratingStream.filter(s -> isInThreshold(getFunny_SadSongAvg(s),funny_sad,threshold))
                    .filter(s -> isInThreshold(getHard_SoftSongAvg(s),hard_soft,threshold))
                    .filter(s -> isInThreshold(getLight_DeepSongAvg(s),light_deep,threshold)).distinct();

        var allFound = fitting.boxed().collect(Collectors.toList());
        Collections.shuffle(allFound);
        var ids = allFound.stream().limit(count);

        return StreamSupport.stream(ids.spliterator(),false).collect(Collectors.toList());
    }

    public void addRating(Long userId, Long songId, Integer funny_sad, Integer light_deep, Integer hard_soft)
    {
        if(!ratings.findAllBySongIdAndUserId(userId,songId).isEmpty())
            ratings.deleteBySongIdAndUserId(userId,songId);

        ratings.save(new SongRating(-1,userId,songId,funny_sad,light_deep,hard_soft));
    }
    public boolean removeRating(Long userId, Long songId)
    {
        if(ratings.findAllBySongIdAndUserId(userId, songId).isEmpty())
            return false;

        ratings.deleteBySongIdAndUserId(userId,songId);
        return true;
    }
    public void removerRatings(Long songId)
    {
        ratings.deleteAllBySongId(songId);
    }

    private Boolean isInThreshold(Integer val, Integer compare, Integer threshold)
    {
        if(compare < val - threshold)
            return false;
        if(compare > val + threshold)
            return false;

        return true;
    }

    public void wipeDB()
    {
        ratings.deleteAll();
    }
}
