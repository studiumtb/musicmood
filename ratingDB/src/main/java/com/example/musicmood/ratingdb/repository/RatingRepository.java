package com.example.musicmood.ratingdb.repository;

import com.example.musicmood.ratingdb.entity.SongRating;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RatingRepository extends CrudRepository<SongRating, Long> {
    List<SongRating> findAllBySongIdAndUserId(Long SongId, Long UserId);
    List<SongRating> findAllBySongId(Long SongId);
    List<SongRating> findAllByUserId(Long UserId);

    void deleteBySongIdAndUserId(Long SongId, Long UserId);
    void deleteAllBySongId(Long SongId);
}