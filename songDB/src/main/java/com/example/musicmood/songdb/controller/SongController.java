package com.example.musicmood.songdb.controller;

import com.example.musicmood.songdb.remoteCalls.RemoteCallsRatingDB;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.example.musicmood.songdb.entity.Song;
import com.example.musicmood.songdb.service.SongDBService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.StreamSupport;

@RestController
@Slf4j
@RequestMapping("/Songs")
@RequiredArgsConstructor
public class SongController extends SongControllerBase {

    private final SongDBService service;

    @GetMapping
    public ResponseEntity<Iterable<Song>> getSongs()
    {
        log.info("GET Full Song List");
        return ResponseEntity.ok(service.getAllSongs());
    }

    @DeleteMapping
    public ResponseEntity wipeDB()
    {
        service.wipeDB();
        return ResponseEntity.ok().build();
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Song> getSongWithId(@PathVariable(name="id") Integer id)
    {
        log.info("GET - get Song with id={}",id);
        try {
            Optional<Song> song = service.getSongsById(id);
            if(song.isPresent())
                return ResponseEntity.ok(song.get());
            else
                return ResponseEntity.notFound().build();
        }
        catch (Exception ex)
        {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/id/{id}")
    public ResponseEntity<Song> deleteSongWithId(@PathVariable(name="id") Integer id)
    {
        log.info("DELETE - delete Song with Id={}", id);
        if(service.getSongsById(id).isPresent())
        {
            try
            {
                ratingDB.removeRatings(id);
            }
            catch (IOException iox)
            {
                log.warn("Error trying to delete Ratings for Song with Id={}", id);
            }
            service.deleteSongById(id);
            return ResponseEntity.ok().build();
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/name/{artist}/{name}")
    public ResponseEntity<Song> getSongWithName(@PathVariable(name="name") String name, @PathVariable(name="artist") String artist)
    {
        log.info("Get - find Songs with name={}",name);
        try
        {
            var song = getSong(name,artist);
            if(song.isPresent())
                return ResponseEntity.ok(song.get());
            else
                return ResponseEntity.notFound().build();
        }
        catch (Exception ex)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/name/{artist}/{name}")
    public ResponseEntity deleteSongWithName(@PathVariable("name") String name, @PathVariable("artist") String artist)
    {
        log.info("Delete Song");
        if(songExists(name,artist))
        {
            try
            {
                var song = getSong(name,artist);
                if(song.isPresent())
                    ratingDB.removeRatings(song.get().getId());
            }
            catch (Exception ex)
            {
                return ResponseEntity.internalServerError().build();
            }
            finally
            {
                service.deleteSongByNameAndArtist(name, artist);
            }

            return ResponseEntity.ok().build();
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/name/{artist}/{name}")
    public ResponseEntity addSong(@PathVariable("name") String name, @PathVariable("artist") String artist, @RequestParam("length") Double length)
    {
        if(!songExists(name,artist))
        {
            log.info("ADD New Song");
            service.addSong(name, artist, length);
            return ResponseEntity.ok().build();
        }
        else
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }

    private Boolean songExists(String name, String artist)
    {
        var song = service.getSongsByNameAndArtist(name,artist);
        if(StreamSupport.stream(song.spliterator(),false).count() == 0)
            return false;
        else
            return true;
    }
    private Optional<Song> getSong(String name, String artist)
    {
        var song = service.getSongsByNameAndArtist(name,artist);
        return StreamSupport.stream(song.spliterator(),false).findAny();
    }
}

class SongControllerBase
{
    RemoteCallsRatingDB ratingDB = new RemoteCallsRatingDB();
}
