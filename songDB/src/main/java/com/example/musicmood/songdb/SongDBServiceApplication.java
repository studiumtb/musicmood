package com.example.musicmood.songdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SongDBServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SongDBServiceApplication.class, args);
    }

}
