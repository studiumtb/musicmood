package com.example.musicmood.songdb.service;

import com.example.musicmood.songdb.entity.Song;
import com.example.musicmood.songdb.repository.SongDBRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SongDBService
{
    @Autowired
    private final SongDBRepository songs;

    public Iterable<Song> getAllSongs()
    {
        return songs.findAll();
    }

    public Iterable<Song> getSongsByNameAndArtist(String name, String artist) {
        return songs.findAllByNameAndArtist(name,artist);
    }
    public Optional<Song> getSongsById(Integer id) {
        return songs.findSongById(id);
    }

    public void addSong(Song song)
    {
        songs.save(song);
    }
    public void addSong(String name, String artist, double length)
    {
        Song song = new Song();
        song.setName(name);
        song.setArtist(artist);
        song.setLength(length);

        songs.save(song);
    }
    public void deleteSongById(Integer id) { songs.deleteById(id);}
    public void deleteSongByNameAndArtist(String name, String artist)
    {
        List<Song> allMatching = songs.findAllByNameAndArtist(name, artist);
        if(!allMatching.isEmpty())
        {
            songs.deleteById(allMatching.stream().findFirst().get().getId());
        }
    }
    public void wipeDB()
    {
        songs.deleteAll();
    }
}
