package com.example.musicmood.songdb.repository;

import com.example.musicmood.songdb.entity.Song;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SongDBRepository extends CrudRepository<Song, Integer> {
    List<Song> findAllByName(String name);
    List<Song> findAllByNameAndArtist(String name, String artist);
    Optional<Song> findSongById(Integer id);
}
