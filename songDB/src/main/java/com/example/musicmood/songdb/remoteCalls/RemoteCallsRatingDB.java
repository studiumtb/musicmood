package com.example.musicmood.songdb.remoteCalls;

import javassist.NotFoundException;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.rmi.ServerException;

public class RemoteCallsRatingDB {
    public boolean removeRatings(long songid) throws IOException {
        URL url = new URL("http://localhost:8083/song/" + songid);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod("DELETE");

        int status = request.getResponseCode();
        if(status == HttpStatus.OK.value())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
