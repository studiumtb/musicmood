import Vue from 'vue';
import VueRouter from 'vue-router';
import VueCookies from 'vue-cookies';
import Login from '../views/Login.vue';
import Song from '../views/Song.vue';
import Add from '../views/Add.vue';

Vue.use(VueRouter);
Vue.use(VueCookies);

const routes = [
  {
    path: '/',
    name: 'LoginScreen',
    component: Login,
  },
  {
    path: '/songs',
    name: 'songs',
    component: Song,
  },
  {
    path: '/add',
    name: 'add',
    component: Add,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
